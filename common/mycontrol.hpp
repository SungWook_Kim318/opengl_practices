#ifndef MYCONTROL_HPP

void computeMatricesFromInputs();
glm::mat4 getViewMatrix();
glm::mat4 getProjectionMatrix();

float getMouseSpeed(void);
void SetMouseSpeed(float f);
#endif // !MYCONTROL_HPP
