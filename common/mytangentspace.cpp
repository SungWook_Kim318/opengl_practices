#include "mytangentspace.hpp"



void computeTangentBasis(
    // inputs
    const std::vector<glm::vec3> & vertices,
    const std::vector<glm::vec2> & uvs,
    const std::vector<glm::vec3> & normals,
    // outputs
    std::vector<glm::vec3> & tangents,
    std::vector<glm::vec3> & bitangents
)
{
    // fit the size of tangent and bitangent
    tangents.resize(vertices.size());
    bitangents.resize(vertices.size());

    for( unsigned i = 0; i < vertices.size(); i += 3 )
    {
        // Edges of the triangle : postion delta
        const glm::vec3 dv1 = vertices[i + 1] - vertices[i + 0];
        const glm::vec3 dv2 = vertices[i + 2] - vertices[i + 0];

        // UV delta
        const glm::vec2 duv1 = uvs[i + 1] - uvs[i + 0];
        const glm::vec2 duv2 = uvs[i + 2] - uvs[i + 0];

        const float r = 1.0f / (duv1.x * duv2.y - duv1.y * duv2.x);
        glm::vec3 t = (dv1 * duv2.y - dv2 * duv1.y) * r;
        glm::vec3 b = (dv2 * duv1.x - dv1 * duv2.x) * r;

        for( unsigned j = 0 ; j < 3 ; ++j )
        {
            // get normal vector of vertex(normal is different with each vertax)
            const glm::vec3 & n = normals[i + j];

            // Gram-Schmidt orthogonalize
            t = glm::normalize(t - n * glm::dot(n, t));

            if( glm::dot(glm::cross(n, t), b) < 0.f )
            {
                t *= -1.f;
            }
            // Add tangent & bitangent vectors in vector
            tangents[j + i] = t;
            bitangents[j + i] = glm::normalize(b);
        }
    }
}