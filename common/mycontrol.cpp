#include <glfw3.h>
extern GLFWwindow* window; // The "extern" keyword here is to access the variable "window" declared in tutorialXXX.cpp. This is a hack to keep the tutorials simple. Please avoid this.

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
using namespace glm;

#include "mycontrol.hpp"

static glm::mat4 ViewMatrix;
static glm::mat4 ProjectionMatrix;

glm::mat4 getViewMatrix() {
  return ViewMatrix;
}
glm::mat4 getProjectionMatrix() {
  return ProjectionMatrix;
}

// position
static glm::vec3 position = glm::vec3(0, 0, 10);
// horizontal angle : toward -Z
static float horizontalAngle = 3.14f;
// vertical angle : 0, look at the horizon
static float verticalAngle = 0.f;
// Initial Field of View
static float initialFoV = 45.0f;

static float speed = 3.0f; // 3 units / second

static float mouseSpeed = 0.005f;

extern int width;
extern int height;

void computeMatricesFromInputs(void)
{
  static double lastTime = glfwGetTime();

  double currentTime = glfwGetTime();
  float deltaTime = float(currentTime - lastTime);

  // Get mouse position
  double xpos, ypos;
  glfwGetCursorPos(window, &xpos, &ypos);

  // Reset mouse position for next frame
  glfwSetCursorPos(window, width / 2, height / 2);

  // Compute orientation by mouse's movement
  // mouse's x-direction move
  horizontalAngle += mouseSpeed * float(width / 2 - xpos);
  // mouse's y-direction move
  verticalAngle += mouseSpeed * float(height / 2 - ypos);

  // Calculate Drection vector(Camera's looking position)
  // Direction : Spherical coordinates to Cartesian coordinates conversion
  glm::vec3 direction(
    cos(verticalAngle) * sin(horizontalAngle),
    sin(verticalAngle),
    cos(verticalAngle) * cos(horizontalAngle)
  );

  // Right vector
  // camera's right vector(relation with NDC)
  glm::vec3 right = glm::vec3(
    sin(horizontalAngle - 3.14f / 2.0f),
    0,
    cos(horizontalAngle - 3.14f / 2.0f)
  );

  // Calculate Up vector(relation with NDC)
  //  glm::vec3 up = glm::vec3(
  //    0,
  //    cos(verticalAngle),
  //    sin(verticalAngle)
  //  );
  glm::vec3 up = glm::cross(right, direction);

    // Move forward
  if (glfwGetKey(window, GLFW_KEY_UP) == GLFW_PRESS) {
    position += direction * deltaTime * speed;
  }
  // Move backward
  if (glfwGetKey(window, GLFW_KEY_DOWN) == GLFW_PRESS) {
    position -= direction * deltaTime * speed;
  }
  // Strafe right
  if (glfwGetKey(window, GLFW_KEY_RIGHT) == GLFW_PRESS) {
    position += right * deltaTime * speed;
  }
  // Strafe left
  if (glfwGetKey(window, GLFW_KEY_LEFT) == GLFW_PRESS) {
    position -= right * deltaTime * speed;
  }
  float FoV = initialFoV;// - 5 * glfwGetMouseWheel(); // Now GLFW 3 requires setting up a callback for this. It's a bit too complicated for this beginner's tutorial, so it's disabled instead.


  // Projection matrix : 45?Field of View, 4:3 ratio, display range : 0.1 unit <-> 100 units
  ProjectionMatrix = glm::perspective(glm::radians(FoV), 4.0f / 3.0f, 0.1f, 100.0f);
  // Camera matrix
  ViewMatrix = glm::lookAt(position,             // Camera is here
                           position + direction, // and looks here : at the same position, plus "direction"
                           up                    // Head is up (set to 0,-1,0 to look upside-down)
  );

  // For the next frame, the "last time" will be "now"
  lastTime = currentTime;
}


float getMouseSpeed(void)
{
	return speed;
}
void SetMouseSpeed(float f)
{
	speed = f;
}
