//standard library
#include <stdio.h>
#include <stdlib.h>

// Standard template library
#include <vector>

#include <GL/glew.h>

#include <glfw3.h>

GLFWwindow* window;

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
using namespace glm;

#include "common/shader.hpp"
#include "common/texture.hpp"
#include "common/mycontrol.hpp"
#include "common/myObjloader.hpp"
#include "common/myText2D.hpp"

// Global data
int width = 1024;
int height = 768;


int main(void)
{
    // Initialise GLFW
    if( !glfwInit() )
    {
        fprintf(stderr, "%d: Failed to initialize GLFW\n", __LINE__);
        getchar();
        return -1;
    }

    glfwWindowHint(GLFW_SAMPLES, 8); // MSAAx8
                                     //glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    //_MSC_VER
#ifdef __clang__
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE); // To make MacOS happy; should not be needed
#endif
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    //glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_COMPAT_PROFILE); // So that glBegin/glVertex/glEnd work

    // Open a window and create its OpenGL context
    window = glfwCreateWindow(width, height, "Project : OpenMind", NULL, NULL);
    if( window == NULL )
    {
        fprintf(stderr, "%d: Failed to open GLFW window. If you have an Intel GPU, they are not 3.3 compatible. Try the 2.1 version of the tutorials.\n", __LINE__);
        getchar();
        glfwTerminate();
        return -1;
    }

    glfwMakeContextCurrent(window); // Initialize GLEW(openGl context create)

    glewExperimental = true; // Needed for core profile
    if( glewInit() != GLEW_OK )
    {
        fprintf(stderr, "%d: Failed to initialize GLEW\n", __LINE__);
        getchar();
        glfwTerminate();
        return -1;
    }

    // Ensure we can capture the escape key being pressed below
    glfwSetInputMode(window, GLFW_STICKY_KEYS, GL_TRUE);
    // Hide the mouse and enable unlimited mouvement
    glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);

    // Set the mouse at the center of the screen
    glfwPollEvents();
    glfwSetCursorPos(window, width / 2, height / 2);

    // Dark background
    glClearColor(0.0f, 0.0f, 0.4f, 0.0f);

    // Change setting
    // Enable depth test
    glEnable(GL_DEPTH_TEST);
    // Accept fragment if it closer to the camera than the former one
    glDepthFunc(GL_LESS);

    // Cull triangles which normal is not towards the camera
    glEnable(GL_CULL_FACE); //for Transparency -> off.

    // init BUFFERS!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    // alloc and init VAO
    GLuint VertexArrayID;
    glGenVertexArrays(1, &VertexArrayID);
    glBindVertexArray(VertexArrayID);

    // Load and compile shaders file in program
    GLuint programID = //LoadShaders("NormalMapping.vertexshader", "NormalMapping.fragmentshader");
        LoadShaders("shader/15/basic.vert", "shader/15/basicLOD.frag");

    // Add mvp
    GLuint mvpid = glGetUniformLocation(programID, "MVP");
    
    // Load Texture image file
    GLuint Texture = loadDDS("image/15/lightmap.DDS");
    
    // Get a handle for our textures uniform
    GLuint TextureID = glGetUniformLocation(programID, "myTextureSampler");

    // Load .obj file
    std::vector< glm::vec3 > vertices;
    std::vector< glm::vec2 > uvs;
    std::vector< glm::vec3 > normals;

    if( !loadOBJ("image/15/room.obj", vertices, uvs, normals) )
    {
        printf("ERROR: %s : %d, loading in obj file", __FILE__, __LINE__);
        getchar();
        return -1;
    }

    // Load data into a VBO
    GLuint vertexbuffer;
    glGenBuffers(1, &vertexbuffer);
    glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
    glBufferData(GL_ARRAY_BUFFER,
                 vertices.size() * sizeof(glm::vec3),
                 vertices.data(),
                 GL_STATIC_DRAW);

    // alloc uv buffer
    GLuint uvbuffer;
    glGenBuffers(1, &uvbuffer);
    glBindBuffer(GL_ARRAY_BUFFER, uvbuffer);
    glBufferData(GL_ARRAY_BUFFER,
                 uvs.size() * sizeof(glm::vec2),
                 uvs.data(),
                 GL_STATIC_DRAW);

    // Initialize our little text library with the Holstein font
    initText2D("image/text/Holstein.DDS");

    // For speed computation
    double lastTime = glfwGetTime();
    int nbFrames = 0;

    // Enable blending for transperate -> turn off
    // glEnable(GL_BLEND);
    // glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    do
    {
        // Measure speed
        double currentTime = glfwGetTime();
        nbFrames++;
        if( currentTime - lastTime >= 1.0 )
        {
            nbFrames = 0;
            lastTime += 1.0;
        }
        // clear screen
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        // use our shader
        glUseProgram(programID);

        // Compute the MVP matrix from keyboard and mouse input
        computeMatricesFromInputs();
        glm::mat4 ProjectionMatrix = getProjectionMatrix();
        glm::mat4 ViewMatrix = getViewMatrix();
        glm::mat4 ModelMatrix1 = glm::mat4(1.0);
        glm::mat4 mvp1 = ProjectionMatrix * ViewMatrix * ModelMatrix1;

        // Send our "mvp" unifrom mtx.
        glUniformMatrix4fv(mvpid, 1, GL_FALSE, &mvp1[0][0]);

        // Bind our texture in Texture Unit 0
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, Texture);
        // Set our "DiffuseTextureSampler" sampler to use Texture Unit 0
        glUniform1i(TextureID, 0);

        // Draw vetex in buffer
        // 1st attrivute : vertexs
        glEnableVertexAttribArray(0);
        glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
        glVertexAttribPointer(
            0,            // 0th attribute, it must fit with layout of shader
            3,            // size of each point(vec3)
            GL_FLOAT,     // type
            GL_FALSE,     // normalized -> no
            0,            // stride of next
            (void*)0      // array buffer's offeset(moving value)
        );

        // 2nd attribute : UVs
        glEnableVertexAttribArray(1);
        glBindBuffer(GL_ARRAY_BUFFER, uvbuffer);
        glVertexAttribPointer(
            1,            // 1th attribute
            2,            // size of each point(vec2, U+V)
            GL_FLOAT,     // type
            GL_FALSE,     // normalized -> no
            0,            // not normalized
            (void*)0);    // offest -> no

        // Draw Triangles
        glDrawArrays(GL_TRIANGLES, 0, vertices.size());

        glDisableVertexAttribArray(0);
        glDisableVertexAttribArray(1);

        //Txt setting
        static char text[256];
        sprintf(text, "%.2f sec", glfwGetTime());
        printText2D(text, 10, 500, 60);

        // Swap glfw buffers
        glfwSwapBuffers(window);
        glfwPollEvents();

    } // Check if the ESC key was pressed or the window was closed
    while( glfwGetKey(window, GLFW_KEY_ESCAPE) != GLFW_PRESS &&
          glfwWindowShouldClose(window) == 0 );

    // Cleanup VBO and shader
    glDeleteBuffers(1, &vertexbuffer);
    glDeleteBuffers(1, &uvbuffer);
    glDeleteProgram(programID);
    glDeleteTextures(1, &Texture);
    glDeleteVertexArrays(1, &VertexArrayID);

    // Close OpenGL window and terminate GLFW
    glfwTerminate();

    return 0;
}
