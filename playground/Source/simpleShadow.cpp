//standard library
#include <stdio.h>
#include <stdlib.h>

// Standard template library
#include <vector>
#include <GL/glew.h>
#include <glfw3.h>

GLFWwindow* window;

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
using namespace glm;

#include "common/shader.hpp"
#include "common/texture.hpp"
#include "common/mycontrol.hpp"
#include "common/myObjloader.hpp"
#include "common/myText2D.hpp"
#include "common/vboindexer.hpp"

// Global data
int width = 1024;
int height = 768;


int main(void)
{
    // Initialise GLFW
    if( !glfwInit() )
    {
        fprintf(stderr, "%d: Failed to initialize GLFW\n", __LINE__);
        getchar();
        return -1;
    }

    glfwWindowHint(GLFW_SAMPLES, 8); // MSAAx8
                                     //glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    //_MSC_VER
#ifdef __clang__
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE); // To make MacOS happy; should not be needed
#endif
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    //glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_COMPAT_PROFILE); // So that glBegin/glVertex/glEnd work

    // Open a window and create its OpenGL context
    window = glfwCreateWindow(width, height, "Project : OpenMind", NULL, NULL);
    if( window == NULL )
    {
        fprintf(stderr, "%d: Failed to open GLFW window. If you have an Intel GPU, they are not 3.3 compatible. Try the 2.1 version of the tutorials.\n", __LINE__);
        getchar();
        glfwTerminate();
        return -1;
    }

    glfwMakeContextCurrent(window); // Initialize GLEW(openGl context create)

    glfwGetFramebufferSize(window, &width, &height);
    
    glewExperimental = true; // Needed for core profile
    if( glewInit() != GLEW_OK )
    {
        fprintf(stderr, "%d: Failed to initialize GLEW\n", __LINE__);
        getchar();
        glfwTerminate();
        return -1;
    }

    // Ensure we can capture the escape key being pressed below
    glfwSetInputMode(window, GLFW_STICKY_KEYS, GL_TRUE);
    // Hide the mouse and enable unlimited mouvement
    glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);

    // Set the mouse at the center of the screen
    glfwPollEvents();
    glfwSetCursorPos(window, width / 2, height / 2);

    // Dark background
    glClearColor(0.0f, 0.0f, 0.4f, 0.0f);

    // Change setting
    // Enable depth test
    glEnable(GL_DEPTH_TEST);
    // Accept fragment if it closer to the camera than the former one
    glDepthFunc(GL_LESS);

    // Cull triangles which normal is not towards the camera
    glEnable(GL_CULL_FACE); //for Transparency -> off.

    // init BUFFERS!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    // alloc and init VAO
    GLuint VertexArrayID;
    glGenVertexArrays(1, &VertexArrayID);
    glBindVertexArray(VertexArrayID);

    // Load and compile shaders file in program
    GLuint depthProgramID = //LoadShaders("NormalMapping.vertexshader", "NormalMapping.fragmentshader");
        LoadShaders("shader/16/depthRTT.vert", "shader/16/depthRTT.frag");
    
    // Get a handle for our "MVP" uniform
    GLuint depthMatrixID = glGetUniformLocation(depthProgramID, "depthMVP");

    // Load Texture image file
    GLuint Texture = loadDDS("image/16/uvmap.DDS");

    // Load .obj file
    std::vector< glm::vec3 > vertices;
    std::vector< glm::vec2 > uvs;
    std::vector< glm::vec3 > normals;

    if( !loadOBJ("image/16/room_thickwalls.obj", vertices, uvs, normals) )
    {
        printf("ERROR: %s : %d, loading in obj file", __FILE__, __LINE__);
        getchar();
        return -1;
    }
    
    std::vector<unsigned short> indices;
    std::vector<glm::vec3> indexed_vertices;
    std::vector<glm::vec2> indexed_uvs;
    std::vector<glm::vec3> indexed_normals;
    indexVBO(vertices, uvs, normals, indices, indexed_vertices, indexed_uvs, indexed_normals);

    // Load data into a VBO
    GLuint vertexbuffer;
    glGenBuffers(1, &vertexbuffer);
    glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
    glBufferData(GL_ARRAY_BUFFER,
                 indexed_vertices.size() * sizeof(glm::vec3),
                 indexed_vertices.data(),
                 GL_STATIC_DRAW);

    // alloc uv buffer
    GLuint uvbuffer;
    glGenBuffers(1, &uvbuffer);
    glBindBuffer(GL_ARRAY_BUFFER, uvbuffer);
    glBufferData(GL_ARRAY_BUFFER,
                 indexed_uvs.size() * sizeof(glm::vec2),
                 indexed_uvs.data(),
                 GL_STATIC_DRAW);
    
    GLuint normalbuffer;
    glGenBuffers(1, &normalbuffer);
    glBindBuffer(GL_ARRAY_BUFFER, normalbuffer);
    glBufferData(GL_ARRAY_BUFFER,
                 indexed_normals.size() * sizeof(glm::vec3),
                 indexed_normals.data(),
                 GL_STATIC_DRAW);
    
    // Generate a buffer for the indices as well
    GLuint elementbuffer;
    glGenBuffers(1,&elementbuffer);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, elementbuffer);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER,
                 indices.size() * sizeof(unsigned short),
                 indices.data(),
                 GL_STATIC_DRAW);
    
    

    // Initialize our little text library with the Holstein font
    initText2D("image/text/Holstein.DDS");

    // For speed computation
    double lastTime = glfwGetTime();
    int nbFrames = 0;

    // Enable blending for transperate -> turn off
    // glEnable(GL_BLEND);
    // glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    // ---------------------------------------------
    // Render to Texture - specific code begins here
    // ---------------------------------------------
    GLuint FramebufferName = 0;
    glGenFramebuffers(1, &FramebufferName);
    glBindFramebuffer(GL_FRAMEBUFFER, FramebufferName);

    // Depth texture. Slower than a depth buffer, but you can sample it later in your shader
    GLuint depthTexture;
    glGenTextures(1, &depthTexture);
    glBindTexture(GL_TEXTURE_2D, depthTexture);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT16,
                 1024, 1024,
                 0,GL_DEPTH_COMPONENT, GL_FLOAT, 0);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_FUNC, GL_LEQUAL);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_MODE, GL_COMPARE_R_TO_TEXTURE);
    
    glFramebufferTexture(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, depthTexture, 0);

    // No color output in the bound framebuffer, inly depth
    glDrawBuffer(GL_NONE);

    if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
        return -1;
    
    GLuint programID = LoadShaders("shader/16/ShadowMapping_SimpleVersion.vert", "shader/16/ShadowMapping_SimpleVersion.frag");

    // Get a handle for our "myTextureSampler" uniform
    GLuint TextureID = glGetUniformLocation(programID, "myTextureSampler");
    
    // Get a handle ofr our myTextureSampler uniform
    GLuint MatrixID = glGetUniformLocation(programID, "MVP");
    GLuint DepthBiasID = glGetUniformLocation(programID, "DepthBiasMVP");
    GLuint ShadowMapID = glGetUniformLocation(programID, "shadowMap");
    
    do
    {
        // Measure speed
        double currentTime = glfwGetTime();
        nbFrames++;
        if( currentTime - lastTime >= 1.0 )
        {
            nbFrames = 0;
            lastTime += 1.0;
        }
        
        // Render to our framebuffer
        glBindFramebuffer(GL_FRAMEBUFFER, FramebufferName);
        glViewport(0, 0, 1024, 1024);
        
        // We don't use bias in the shader, but instead we draw back faces,
        // which are already separated from the front faces by a small distance
        // (if your geometry is made this way)
        glEnable(GL_CULL_FACE);
        glCullFace(GL_BACK); // Cull back-facing triangles -> draw only front-facing triangles
        
        // clear screen
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        // use our shader
        glUseProgram(depthProgramID);
        
        glm::vec3 lightInvDir = glm::vec3(0.5f, 2.f, -2.f);
        
        // Computer the MVP matrix form the light's point of view
        glm::mat4 depthProjectionMatrix = glm::ortho<float>(-10, 10, -10, 10, -10, 20);
        glm::mat4 depthViewMatrix = glm::lookAt(lightInvDir, glm::vec3(0,0,0), glm::vec3(0,1,0));
        // or, for spot light :
        //glm::vec3 lightPos(5, 20, 20);
        //glm::mat4 depthProjectionMatrix = glm::perspective<float>(45.0f, 1.0f, 2.0f, 50.0f);
        //glm::mat4 depthViewMatrix = glm::lookAt(lightPos, lightPos-lightInvDir, glm::vec3(0,1,0));
        
        glm::mat4 depthModelMatrix = glm::mat4(1.0);
        glm::mat4 depthMVP = depthProjectionMatrix * depthViewMatrix * depthModelMatrix;
        
        // Send our transformation to the currently bound shader,
        // in the "MVP" uniform
        glUniformMatrix4fv(depthMatrixID, 1, GL_FALSE, &depthMVP[0][0]);
        
        // 1st attribute buffer : vetrices
        glEnableVertexAttribArray(0);
        glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
        glVertexAttribPointer(
                              0,  // The attribute we want to configure
                              3,                  // size
                              GL_FLOAT,           // type
                              GL_FALSE,           // normalized?
                              0,                  // stride
                              (void*)0            // array buffer offset
                              );
        
        // Index buffer
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, elementbuffer);
        
        // Draw the triangles !
        glDrawElements(
                       GL_TRIANGLES,      // mode
                       indices.size(),    // count
                       GL_UNSIGNED_SHORT, // type
                       (void*)0           // element array buffer offset
                       );
        
        glDisableVertexAttribArray(0);
        
        // Render to the screen
        glBindFramebuffer(GL_FRAMEBUFFER, 0);
        glViewport(0, 0, width, height); // Render on the whole framebuffer, complete from the lower left corner to the upper right
        
        glEnable(GL_CULL_FACE);
        glCullFace(GL_BACK); // Cull back-facing triangles -> draw only front-facing triangles
        
        // Clear the screen
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        
        // Use our shader
        glUseProgram(programID);
        
        // Compute the MVP matrix from keyboard and mouse input
        computeMatricesFromInputs();
        glm::mat4 ProjectionMatrix = getProjectionMatrix();
        glm::mat4 ViewMatrix = getViewMatrix();
        glm::mat4 ModelMatrix = glm::mat4(1.0);
        glm::mat4 MVP = ProjectionMatrix * ViewMatrix * ModelMatrix;
        
        glm::mat4 biasMatrix(
                             0.5, 0.0, 0.0, 0.0,
                             0.0, 0.5, 0.0, 0.0,
                             0.0, 0.0, 0.5, 0.0,
                             0.5, 0.5, 0.5, 1.0
                             );
        
        glm::mat4 depthBiasMVP = biasMatrix*depthMVP;
        
        
        // Send our "mvp" unifrom mtx.
        glUniformMatrix4fv(MatrixID, 1, GL_FALSE, &MVP[0][0]);
        glUniformMatrix4fv(DepthBiasID, 1, GL_FALSE, &depthBiasMVP[0][0]);
        

        // Bind our texture in Texture Unit 0
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, Texture);
        // Set our "DiffuseTextureSampler" sampler to use Texture Unit 0
        glUniform1i(TextureID, 0);

        glActiveTexture(GL_TEXTURE1);
        glBindTexture(GL_TEXTURE_2D, depthTexture);
        glUniform1i(ShadowMapID, 1);
        
        // Draw vetex in buffer
        // 1st attrivute : vertexs
        glEnableVertexAttribArray(0);
        glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
        glVertexAttribPointer(
            0,            // 0th attribute, it must fit with layout of shader
            3,            // size of each point(vec3)
            GL_FLOAT,     // type
            GL_FALSE,     // normalized -> no
            0,            // stride of next
            (void*)0      // array buffer's offeset(moving value)
        );

        // 2nd attribute : UVs
        glEnableVertexAttribArray(1);
        glBindBuffer(GL_ARRAY_BUFFER, uvbuffer);
        glVertexAttribPointer(
            1,            // 1th attribute
            2,            // size of each point(vec2, U+V)
            GL_FLOAT,     // type
            GL_FALSE,     // normalized -> no
            0,            // not normalized
            (void*)0);    // offest -> no

        // 3rd attribute buffer : normals
        glEnableVertexAttribArray(2);
        glBindBuffer(GL_ARRAY_BUFFER, normalbuffer);
        glVertexAttribPointer(
                              2,                                // attribute
                              3,                                // size
                              GL_FLOAT,                         // type
                              GL_FALSE,                         // normalized?
                              0,                                // stride
                              (void*)0                          // array buffer offset
                              );
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, elementbuffer);
        
        // Draw Triangles
        glDrawElements(GL_TRIANGLES,        // mode
                       indices.size(),      // size of buffer
                       GL_UNSIGNED_SHORT,   // type
                       (void*)0);           // array buffer offset

        glDisableVertexAttribArray(0);
        glDisableVertexAttribArray(1);
        glDisableVertexAttribArray(2);


        //Txt setting
        static char text[256];
        sprintf(text, "%.2f sec", glfwGetTime());
        printText2D(text, 10, 500, 60);

        // Swap glfw buffers
        glfwSwapBuffers(window);
        glfwPollEvents();

    } // Check if the ESC key was pressed or the window was closed
    while( glfwGetKey(window, GLFW_KEY_ESCAPE) != GLFW_PRESS &&
          glfwWindowShouldClose(window) == 0 );

    // Cleanup VBO and shader
    glDeleteBuffers(1, &vertexbuffer);
    glDeleteBuffers(1, &uvbuffer);
    glDeleteBuffers(1, &normalbuffer);
    glDeleteBuffers(1, &elementbuffer);
    glDeleteProgram(programID);
    glDeleteProgram(depthProgramID);
    glDeleteTextures(1, &Texture);
    
    glDeleteFramebuffers(1, &FramebufferName);
    glDeleteTextures(1, &depthTexture);

    // Close OpenGL window and terminate GLFW
    glfwTerminate();

    return 0;
}
