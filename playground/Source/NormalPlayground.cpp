//standard library
#include <stdio.h>
#include <stdlib.h>

// Standard template library
#include <vector>

#include <GL/glew.h>

#include <glfw3.h>

GLFWwindow* window;

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
using namespace glm;

#include "common/shader.hpp"
#include "common/texture.hpp"
#include "common/mycontrol.hpp"
#include "common/myobjloader.hpp"
#include "common/vboindexer.hpp"
#include "common/myText2D.hpp"
#include "common/mytangentspace.hpp"

// Global data
int width = 1024;
int height = 768;


int main(void)
{
    // Initialise GLFW
    if( !glfwInit() )
    {
        fprintf(stderr, "%d: Failed to initialize GLFW\n", __LINE__);
        getchar();
        return -1;
    }

    glfwWindowHint(GLFW_SAMPLES, 1); // No Sampling
                                     //glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    //_MSC_VER
#ifdef __clang__
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE); // To make MacOS happy; should not be needed
#endif
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    //glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_COMPAT_PROFILE); // So that glBegin/glVertex/glEnd work

    // Open a window and create its OpenGL context
    window = glfwCreateWindow(width, height, "Project : OpenMind", NULL, NULL);
    if( window == NULL )
    {
        fprintf(stderr, "%d: Failed to open GLFW window. If you have an Intel GPU, they are not 3.3 compatible. Try the 2.1 version of the tutorials.\n", __LINE__);
        getchar();
        glfwTerminate();
        return -1;
    }

    glfwMakeContextCurrent(window); // Initialize GLEW(openGl context create)

    glewExperimental = true; // Needed for core profile
    if( glewInit() != GLEW_OK )
    {
        fprintf(stderr, "%d: Failed to initialize GLEW\n", __LINE__);
        getchar();
        glfwTerminate();
        return -1;
    }

    // Ensure we can capture the escape key being pressed below
    glfwSetInputMode(window, GLFW_STICKY_KEYS, GL_TRUE);
    // Hide the mouse and enable unlimited mouvement
    glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);

    // Set the mouse at the center of the screen
    glfwPollEvents();
    glfwSetCursorPos(window, width / 2, height / 2);

    // Dark background
    glClearColor(0.0f, 0.0f, 0.4f, 0.0f);

    // Change setting
    // Enable depth test
    glEnable(GL_DEPTH_TEST);
    // Accept fragment if it closer to the camera than the former one
    glDepthFunc(GL_LESS);

    // Cull triangles which normal is not towards the camera
    glEnable(GL_CULL_FACE); //for Transparency -> off.

                            // init BUFFERS!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                            // alloc and init VAO
    GLuint VertexArrayID;
    glGenVertexArrays(1, &VertexArrayID);
    glBindVertexArray(VertexArrayID);

    // Load and compile shaders file in program
    GLuint programID = //LoadShaders("NormalMapping.vertexshader", "NormalMapping.fragmentshader");
        LoadShaders("normal.vert", "normal.frag");

    // Add mvp
    GLuint mvpid = glGetUniformLocation(programID, "MVP");
    GLuint ViewMatrixID = glGetUniformLocation(programID, "V");
    GLuint ModelMatrixID = glGetUniformLocation(programID, "M");
    GLuint ModelView3x3MatrixID = glGetUniformLocation(programID, "MV3x3");

    // Load Texture image file
    GLuint DiffuseTexture = loadDDS("image/diffuse.DDS");
    GLuint NormalTexture = loadBMP_custom("image/normal.bmp");
    GLuint SpecularTexture = loadDDS("image/specular.DDS");

    // Get a handle for our textures uniform
    GLuint DiffuseTextureID = glGetUniformLocation(programID, "DiffuseTextureSampler");
    GLuint NormalTextureID = glGetUniformLocation(programID, "NormalTextureSampler");
    GLuint SpecularTextureID = glGetUniformLocation(programID, "SpecularTextureSampler");

    // Load .obj file
    std::vector< glm::vec3 > vertices;
    std::vector< glm::vec2 > uvs;
    std::vector< glm::vec3 > normals;

    if( !loadOBJ("image/cylinder.obj", vertices, uvs, normals) )
    {
        printf("ERROR: %s : %d, loading in obj file", __FILE__, __LINE__);
        getchar();
        return -1;
    }

    std::vector<glm::vec3> tangents;
    std::vector<glm::vec3> bitangents;
    computeTangentBasis(
        vertices, uvs, normals, // input
        tangents, bitangents    // output
    );

    // indexing
    std::vector< unsigned short > indices;
    std::vector< glm::vec3 > indexed_vertices;
    std::vector< glm::vec2 > indexed_uvs;
    std::vector< glm::vec3 > indexed_normals;
    std::vector<glm::vec3> indexed_tangents;
    std::vector<glm::vec3> indexed_bitangents;
    indexVBO_TBN(
        vertices, uvs, normals, tangents, bitangents,
        indices, indexed_vertices, indexed_uvs, indexed_normals, indexed_tangents, indexed_bitangents
    );

    // Load data into a VBO
    // VBO Init and make
    GLuint vertexbuffer;
    glGenBuffers(1, &vertexbuffer);
    glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
    glBufferData(GL_ARRAY_BUFFER,
                 indexed_vertices.size() * sizeof(glm::vec3),
                 indexed_vertices.data(),
                 GL_STATIC_DRAW);

    // alloc uv buffer
    GLuint uvbuffer;
    glGenBuffers(1, &uvbuffer);
    glBindBuffer(GL_ARRAY_BUFFER, uvbuffer);
    glBufferData(GL_ARRAY_BUFFER,
                 indexed_uvs.size() * sizeof(glm::vec2),
                 indexed_uvs.data(),
                 GL_STATIC_DRAW);

    // alloc normal buffer
    GLuint normalbuffer;
    glGenBuffers(1, &normalbuffer);
    glBindBuffer(GL_ARRAY_BUFFER, normalbuffer);
    glBufferData(GL_ARRAY_BUFFER,
                 indexed_normals.size() * sizeof(glm::vec3),
                 indexed_normals.data(),
                 GL_STATIC_DRAW);

    GLuint tangentbuffer;
    glGenBuffers(1, &tangentbuffer);
    glBindBuffer(GL_ARRAY_BUFFER, tangentbuffer);
    glBufferData(GL_ARRAY_BUFFER,
                 indexed_tangents.size() * sizeof(glm::vec3),
                 indexed_tangents.data(),
                 GL_STATIC_DRAW);

    GLuint bitangentbuffer;
    glGenBuffers(1, &bitangentbuffer);
    glBindBuffer(GL_ARRAY_BUFFER, bitangentbuffer);
    glBufferData(GL_ARRAY_BUFFER,
                 indexed_bitangents.size() * sizeof(glm::vec3),
                 indexed_bitangents.data(),
                 GL_STATIC_DRAW);

    // alloc Index buffer
    GLuint elementbuffer;
    glGenBuffers(1, &elementbuffer);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, elementbuffer);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER,
                 indices.size() * sizeof(unsigned short),
                 indices.data(),
                 GL_STATIC_DRAW);

    // Get a handle for our "LightPosition" uniform
    glUseProgram(programID);
    GLuint LightID = glGetUniformLocation(programID, "LightPosition_worldspace");


    // Initialize our little text library with the Holstein font
    initText2D("Holstein.DDS");

    // For speed computation
    double lastTime = glfwGetTime();
    int nbFrames = 0;

    // Enable blending for transperate -> turn off
    // glEnable(GL_BLEND);
    // glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    do
    {
        // Measure speed
        double currentTime = glfwGetTime();
        nbFrames++;
        if( currentTime - lastTime >= 1.0 )
        {
            nbFrames = 0;
            lastTime += 1.0;
        }
        // clear screen
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        // use our shader
        glUseProgram(programID);

        // Compute the MVP matrix from keyboard and mouse input
        computeMatricesFromInputs();
        glm::mat4 ProjectionMatrix = getProjectionMatrix();
        glm::mat4 ViewMatrix = getViewMatrix();
        glm::mat4 ModelMatrix1 = glm::mat4(1.0);
        glm::mat3 ModelView3x3Matrix = glm::mat3(ViewMatrix * ModelMatrix1);
        glm::mat4 mvp1 = ProjectionMatrix * ViewMatrix * ModelMatrix1;

        // Send our "mvp" unifrom mtx.
        glUniformMatrix4fv(mvpid, 1, GL_FALSE, &mvp1[0][0]);
        glUniformMatrix4fv(ModelMatrixID, 1, GL_FALSE, &ModelMatrix1[0][0]);
        glUniformMatrix4fv(ViewMatrixID, 1, GL_FALSE, &ViewMatrix[0][0]);
        glUniformMatrix3fv(ModelView3x3MatrixID, 1, GL_FALSE, &ModelView3x3Matrix[0][0]);


        glm::vec3 lightPos = glm::vec3(0, 0, 4);
        glUniform3f(LightID, lightPos.x, lightPos.y, lightPos.z);

        // Bind our texture in Texture Unit 0
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, DiffuseTexture);
        // Set our "DiffuseTextureSampler" sampler to use Texture Unit 0
        glUniform1i(DiffuseTextureID, 0);

        // Bind our texture in Texture Unit 1
        glActiveTexture(GL_TEXTURE1);
        glBindTexture(GL_TEXTURE_2D, NormalTexture);
        // Set our "NormalTextureSampler" sampler to use Texture Unit 0
        glUniform1i(NormalTextureID, 1);

        // Bind our texture in Texture Unit 2
        glActiveTexture(GL_TEXTURE2);
        glBindTexture(GL_TEXTURE_2D, SpecularTexture);
        // Set our "SpecularTextureSampler" sampler to use Texture Unit 0
        glUniform1i(SpecularTextureID, 2);

        // Draw vetex in buffer
        // 1st attrivute : vertexs
        glEnableVertexAttribArray(0);
        glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
        glVertexAttribPointer(
            0,            // 0th attribute, it must fit with layout of shader
            3,            // size of each point(vec3)
            GL_FLOAT,     // type
            GL_FALSE,     // normalized -> no
            0,            // stride of next
            (void*)0      // array buffer's offeset(moving value)
        );

        // 2nd attribute : colors
        glEnableVertexAttribArray(1);
        glBindBuffer(GL_ARRAY_BUFFER, uvbuffer);
        glVertexAttribPointer(
            1,            // 1th attribute
            2,            // size of each point(vec2, U+V)
            GL_FLOAT,     // type
            GL_FALSE,     // normalized -> no
            0,            // not normalized
            (void*)0);    // offest -> no

                          // 3rd attribute : normals
        glEnableVertexAttribArray(2);
        glBindBuffer(GL_ARRAY_BUFFER, normalbuffer);
        glVertexAttribPointer(
            2,                                // attribute
            3,                                // size
            GL_FLOAT,                         // type
            GL_FALSE,                         // normalized?
            0,                                // stride
            (void*)0                          // array buffer offset
        );
        // 4th attribute : tangents
        glEnableVertexAttribArray(3);
        glBindBuffer(GL_ARRAY_BUFFER, tangentbuffer);
        glVertexAttribPointer(
            3,                                // attribute
            3,                                // size
            GL_FLOAT,                         // type
            GL_FALSE,                         // normalized?
            0,                                // stride
            (void*)0                          // array buffer offset
        );

        // 5th attribute : bitangents
        glEnableVertexAttribArray(4);
        glBindBuffer(GL_ARRAY_BUFFER, bitangentbuffer);
        glVertexAttribPointer(
            4,                                // attribute
            3,                                // size
            GL_FLOAT,                         // type
            GL_FALSE,                         // normalized?
            0,                                // stride
            (void*)0                          // array buffer offset
        );

        // Binding index buffer
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, elementbuffer);

        // Draw the triangles !
        glDrawElements(
            GL_TRIANGLES,      // mode
            indices.size(),    // count
            GL_UNSIGNED_SHORT,   // type
            (void*)0           // element array buffer offset
        );


        ////// End of rendering of the first object //////
        ////// Start of the rendering of the second object //////
        //
        // In our very specific case, the 2 objects use the same shader.
        // So it's useless to re-bind the "programID" shader, since it's already the current one.
        //glUseProgram(programID);
        //
        // Similarly : don't re-set the light position and camera matrix in programID,
        // it's still valid !
        // *** You would have to do it if you used another shader ! ***
        //glUniform3f(LightID, lightPos.x, lightPos.y, lightPos.z);
        //glUniformMatrix4fv(ViewMatrixID, 1, GL_FALSE, &ViewMatrix[0][0]); // This one doesn't change between objects, so this can be done once for all objects that use "programID"
        //
        // Again : this is already done, but this only works because we use the same shader.
        //// Bind our texture in Texture Unit 0
        //glActiveTexture(GL_TEXTURE0);
        //glBindTexture(GL_TEXTURE_2D, Texture);
        //// Set our "myTextureSampler" sampler to use Texture Unit 0
        //glUniform1i(TextureID, 0);

        /* ----------------------- Delete Make Second object --------------------------------
        // Difference Model Matrix and also change mvp(Reuse modeling)
        glm::mat4 ModelMatrix2 = glm::mat4(1.0);
        ModelMatrix2 = glm::translate(ModelMatrix2, glm::vec3(2.0f, 0.f, 0.f));
        glm::mat4 mvp2 = ProjectionMatrix * ViewMatrix * ModelMatrix2;

        // Send our transformation to the currently bound shader,
        // in the "MVP" uniform
        glUniformMatrix4fv(mvpid, 1, GL_FALSE, &mvp2[0][0]);
        glUniformMatrix4fv(ModelMatrixID, 1, GL_FALSE, &ModelMatrix2[0][0]);

        // The rest is exactly the same as the first object

        // 1rst attribute buffer : vertices
        glEnableVertexAttribArray(0);
        glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
        glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);

        // 2nd attribute buffer : UVs
        glEnableVertexAttribArray(1);
        glBindBuffer(GL_ARRAY_BUFFER, uvbuffer);
        glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 0, (void*)0);

        // 3rd attribute buffer : normals
        glEnableVertexAttribArray(2);
        glBindBuffer(GL_ARRAY_BUFFER, normalbuffer);
        glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);

        // Index buffer
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, elementbuffer);

        // Draw the triangles !
        glDrawElements(GL_TRIANGLES, indices.size(), GL_UNSIGNED_SHORT, (void*)0);
        */

        glDisableVertexAttribArray(0);
        glDisableVertexAttribArray(1);
        glDisableVertexAttribArray(2);
        glDisableVertexAttribArray(3);
        glDisableVertexAttribArray(4);

        //Txt setting
        static char text[256];
        sprintf(text, "%.2f sec", glfwGetTime());
        printText2D(text, 10, 500, 60);

        // Swap glfw buffers
        glfwSwapBuffers(window);
        glfwPollEvents();

    } // Check if the ESC key was pressed or the window was closed
    while( glfwGetKey(window, GLFW_KEY_ESCAPE) != GLFW_PRESS &&
          glfwWindowShouldClose(window) == 0 );

    // Cleanup VBO and shader
    glDeleteBuffers(1, &vertexbuffer);
    glDeleteBuffers(1, &uvbuffer);
    glDeleteBuffers(1, &normalbuffer);
    glDeleteBuffers(1, &tangentbuffer);
    glDeleteBuffers(1, &bitangentbuffer);
    glDeleteBuffers(1, &elementbuffer);
    glDeleteProgram(programID);
    glDeleteTextures(1, &DiffuseTexture);
    glDeleteTextures(1, &NormalTexture);
    glDeleteTextures(1, &SpecularTexture);
    glDeleteVertexArrays(1, &VertexArrayID);

    // Close OpenGL window and terminate GLFW
    glfwTerminate();

    return 0;
}
