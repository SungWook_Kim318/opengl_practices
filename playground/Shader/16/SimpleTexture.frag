#version 330 core

// Output data
layout(location = 0) out vec4 color;

uniform sampler2D myTextureSampler;

in vec2 UV;

void main(){
  color = texture(myTextureSampler, UV);
}